import passport from "../lib/passport.js";

export const authenticateMiddleware = async (request, response, next) => {
  try {
    const authHeader = request.headers.authorization;
    if (!authHeader) {
      return response.send({
        message: "fatal error! Token not Found!",
      });
    }
    const [authType, token] = authHeader.split(" ");
    if (authType !== "Bearer") {
      return response.send({
        message: "Corrupt Header",
      });
    }
    await passport.authenticate(
      "jwt",
      { session: false, failWithError: true },
      (error, user) => {
        if (error) {
          return response.send({
            message: "Error! Unable to verify",
          });
        }
        if (!user) {
          return response.send({
            message: "User not found!",
          });
        }
        request.user = user;
        next();
      }
    )(request, response, next);
  } catch (error) {
    return {
      message: "Server not found",
    };
  }
};
