import { Sequelize } from "sequelize";
import { User } from "../models/user.js";
import bcrypt from "bcrypt";
const saltRounds = 10;

export const addUserDataService = async (request) => {
  const { emailId, password, name, city, education } = request.body;

  const hashPassword = await bcrypt.hash(password, saltRounds);

  try {
    const existingUser = await User.findOne({ where: { emailId } });
    if (existingUser) {
      return { error: "User already exists" };
    }

    const user = await User.create({
      emailId,
      password: hashPassword,
      name,
      city,
      education,
    });
    if (!user) {
      return {
        message: "Unsuccessfull registration",
      };
    } else {
      return {
        message: "Successful registration",
      };
    }
  } catch (error) {
    return error.message;
  }
};
