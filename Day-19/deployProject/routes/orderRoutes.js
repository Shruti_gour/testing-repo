import express from "express";
import passport from "passport";

import {
  deleteOrderDataById,
  addUserOrderData,
  getUserSingleOrderData,
  getUserAllOrderData,
} from "../controllers/orderController.js";

import { authenticateMiddleware } from "../middleware/authenticateMiddleware.js";

export const orderRoutes = express.Router();

orderRoutes.use(authenticateMiddleware);

orderRoutes.post("/orders", addUserOrderData);
orderRoutes.get("/orders/:id", getUserSingleOrderData);
orderRoutes.get("/orders", getUserAllOrderData);
orderRoutes.delete("/orders/:id", deleteOrderDataById);
