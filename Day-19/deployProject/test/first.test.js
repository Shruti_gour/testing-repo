import { assert } from "chai";
import { should  } from "chai";
should();

describe("Array", function () {
  describe("#indexOf()", function () {
    it("should return -1 when the value is not present", function () {
      assert.equal([1, 2, 3].indexOf(2), 1);
    });
  });
});

describe("first test case", () => {
  let name = "shruti";
  it("check string", () => {
    assert.equal(typeof name, typeof "shruti");
  });
});

const str = () => {
  return "hii";
};

describe("function check", () => {
  it("name function check", () => {
    assert.equal(str(), "hii");
  });
});

describe("string check", () => {
  let name = "hii";
  it("name is string check", () => {
    assert.typeOf(name, "string");
  });
});

describe("should string check ", () => {
  const name = "hii";
  const object = {
    name: "shruti" ,
    lastname: "gour"
  }
  it("string check", () => {
    name.should.be.a("string");
  });

  it("string equal check", () => {
    name.should.equal("hii");
  })

  it("have name property check", () => {
    object.should.have.property("name");
  })
});


