import express, { request, response } from "express";
import { orderRoutes } from "./routes/orderRoutes.js";
import { userRoutes } from "./routes/userRoutes.js";
import { frontEnd } from "./frontEnd.js"

const app = express();
app.use(express.json());

app.get("/", (request, response) => {
  response.send(frontEnd);
});

app.use("/", userRoutes);
app.use("/", orderRoutes);

app.listen(process.env.PORT || 8000, () => {
  console.log("function is running on Port!");
});
