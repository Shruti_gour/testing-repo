import sinon from "sinon";
import { expect} from "chai";

class student {
  constructor(name, lastName, age) {
    (this.name = name), (this.lastName = lastName), (this.age = age);
  }
  
  helloName() {
    let names = this.getName();
    return "hello" + names;
  }

  getName() {
    return this.name;
  }
}

var studentObj = new student("shruti", "gour", "21");

describe("function return check", () => {
  it("function count check", () => {
    let mock = sinon.mock(studentObj);

    const exp = mock.expects('getName');
    exp.exactly(1);
    exp.withArgs();
    studentObj.helloName();
    mock.verify();
  })

})
