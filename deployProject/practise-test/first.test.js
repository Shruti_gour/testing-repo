import { assert } from "chai";
import { should  } from "chai";
should();
import { expect } from "chai";

describe("Array", function () {
  describe("#indexOf()", function () {
    it("should return -1 when the value is not present", function () {
      assert.equal([1, 2, 3].indexOf(2), 1);
    });
  });
});

describe("first test case", () => {
  let name = "shruti";
  it("check string", () => {
    assert.equal(typeof name, typeof "shruti");
  });
});

const str = () => {
  return "hii";
};

describe("function check", () => {
  it("name function check", () => {
    assert.equal(str(), "hii");
  });
});

describe("string check", () => {
  let name = "hii";
  it("name is string check", () => {
    assert.typeOf(name, "string");
  });
});

const name = "hii";
const object = {
  name: "shruti" ,
  lastname: "gour",
  address: {
    current: "fnd",
    permanent: "dfjkdsj",
    phone: ["12343", "6432746"]
  }
}

describe("should string check ", () => {
  it("string check", () => {
    name.should.be.a("string");
  });

  it("string equal check", () => {
    name.should.equal("hii");
  })

  it("have name property check", () => {
    object.should.have.property("name");
  })

  it("string equal check", () => {
    object.should.have.property("name").with.equal("shruti");
  })
});


describe("expect check", () => {
  it("check string", () => {
    expect(name).to.be.a("string");
  });
  it("equal string", () => {
    expect(name).to.equal("hii");
  });
  it("length of string", () => {
    expect(object.name).to.lengthOf(6);
  });

  it("check object length", () => {
    expect(object).to.have.property("name").with.lengthOf(6);
  });

  it("no of property check", () => {
    expect(object).to.have.all.keys("name", "lastname","address");
  });

  it("no of values in property check", () => {
    expect(object).to.have.nested.property("address");
  });

  it("no of values in phone property check", () => {
    expect(object).to.have.nested.property("address.phone[0]");
  });

  it("cureent addrerss name property check", () => {
    expect(object).to.have.nested.include({"address.current": "fnd"});
  });

  it("corect phone number property check", () => {
    expect(object).to.have.nested.include({"address.phone[1]": "6432746"});
  });
})