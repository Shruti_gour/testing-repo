import sinon from "sinon";
import chai from "chai";
import { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);

class student {
  constructor(name, lastName, age) {
    (this.name = name), (this.lastName = lastName), (this.age = age);
  }

  helloName() {
    let names = this.getName();
    return "hello" + names;
  }

  getName() {
    return this.name;
  }

  getAge() {
    const age = new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(this.age), 2000;
      });
    });
    return age;
  }
}

var studentObj = new student("shruti", "gour", "21");

describe("function promise check", (done) => {
  it("normal function check", () => {
    studentObj.getAge().then((result) => {
      expect(result).to.be.equal(21);
      done();
    })
  });

// using chai as promise

  it("promise function", () => {
    return expect(studentObj.getAge()).to.eventually.equal("21");
  })


});
