import sinon from "sinon";
import { expect } from "chai";

class student {
  constructor(name, lastName, age) {
    (this.name = name), (this.lastName = lastName), (this.age = age);
  }
  
  helloName(name) {
    let names = this.getName();
    this.getName();
    return "hello" + names;
  }

  getName() {
    return this.name;
  }
}

var studentObj = new student("shruti", "gour", "21");

describe("function return check", () => {
  it("object get name function return check", () => {
    expect(studentObj.getName()).to.be.equal("shruti");
  })

  it("object function count ", () => {
    const spy = sinon.spy(studentObj, "getName");
    studentObj.helloName();
    expect(spy.calledTwice).to.be.true;
  })

  it("object function argument count", () => {
    const spy = sinon.spy(studentObj, "helloName");
    studentObj.helloName();
    expect(spy.calledWith()).to.be.true;
  })

})
