### Day-21

#### 1.  Read about Mocha testing. unit, UAT, black box etc.
Testing - Testing is the process of evaluating software or a system to determine if it meets the specified requirements and produces the 
desired outcomes. 
- The purpose of testing is to identify defects or issues in the software so that they can be fixed before the software is released to 
users. 
- There are different types of testing such as functional testing, performance testing, security testing, etc. 
- Testing helps to improve the quality and reliability of software

Unit: 
-  a unit refers to the smallest testable part of an application. 
- It is typically a single function, method, or class that performs a specific task within the application. 
- Unit testing involves testing these individual units in isolation from the rest of the application to ensure that they work as expected 
and meet the specified requirements. 

Types of testing:
1. Functional testing:
- Functional testing is a type of software testing that focuses on verifying that the software application under test meets the business 
requirements and specifications. 
- It is also known as black box testing, which means that the tester has no knowledge of the internal code of the application, and only 
examines the outputs produced for a given input.
- The objective of functional testing is to identify and report any bugs or issues that may impact the application's functionality, ease of use, or user experience. It can be done manually or using automated testing tools.

2. Unit testing:
- Unit testing is a type of software testing that focuses on testing individual units of code in isolation, usually at the level of 
functions or methods. The purpose of unit testing is to ensure that each unit of code is working as intended and is free from bugs and 
errors.
- In unit testing, the individual units of code are tested in isolation from the rest of the application.
- The goal is to isolate the unit of code being tested and ensure that it is functioning correctly, regardless of any issues that may 
exist in other parts of the application.
- Unit testing is an important part of the software development process, as it helps to ensure that code is working as intended before it 
is integrated into the larger application.

3. Integration Testing:
-  Integration testing verifies that different modules of a software application can work together correctly and as a complete system. It 
is conducted after unit testing and before system testing.
- Integration testing is a type of software testing that focuses on testing the interfaces between different software components or 
modules to ensure that they work together as expected
- Integration testing can be done in two ways:
1. Top-down integration testing: This testing approach starts with testing the highest-level modules first, and then gradually working 
down to the lower-level modules.
2. Bottom-up integration testing: This testing approach starts with testing the lowest-level modules first, and then gradually working up 
to the higher-level modules.


4. Acceptance Testing:
- Acceptance testing is done by the customers to check whether the delivered products perform the desired tasks or not, as stated in 
requirements.
- It also go further and check the performance of the system and reject changes if certain goals are not met.

5. Alpha testing:
- This is a type of validation testing. It is a type of acceptance testing which is done before the product is released to customers. It 
is typically done by QA people. 
Example: 
When software testing is performed internally within
the organization

6. Beta Testing
- Beta testing is a type of testing that involves giving a product to a group of users who try out the product in the real world and 
provide feedback. The product is not yet released to the public, and this testing phase is usually the last phase of testing before the 
product is released.
- Beta testing is done to identify any bugs or issues with the product that may have been missed during earlier testing phases. Feedback 
from beta testers is used to improve the product before its final release.

7. White box testing:
- White box testing is a software testing method in which the internal structure, design, and implementation of the system being tested 
is known to the tester. 
- The tester has access to the source code of the software and uses this information to design test cases that verify the system’s logic 
and behavior.
- White box testing is also known as clear box testing, structural testing, or code-based testing. 
- It is a type of testing that is generally done during the software development process and is focused on verifying the correctness and 
completeness of the code. 

8. Smoke Testing:
- Smoke testing is a type of software testing that is performed to verify that the critical functionalities of an application are working 
fine without any issues or errors. 
- It is usually performed after every build to make sure that there are no critical issues or errors that could hinder further testing. 
- Smoke testing aims to verify that the application is stable enough for further testing and is performed to ensure that the application 
is working as expected in its initial stages. 
- It is called smoke testing because, just like smoke, if there are any issues or errors, it indicates that something is wrong with the system.

10. Mocha:
- Mocha is a popular JavaScript testing framework that allows developers to write and run tests for their code. 
- It provides a simple, flexible, and expressive syntax for writing tests, along with a powerful and feature-rich API for running them. 
- Mocha supports both asynchronous and synchronous testing, and can run tests in the browser as well as on the server. 
- It is also highly extensible, with a large ecosystem of plugins and extensions available to enhance its functionality.
Example:
```
// add.js
function add(a, b) {
  return a + b;
}
module.exports = add;
```
```
// test.js
const add = require('./add');
const assert = require('assert');

describe('add', function() {
  it('should return the sum of two numbers', function() {
    const result = add(2, 3);
    assert.equal(result, 5);
  });
});

```


11. Chai:
- Chai is an assertion library that is used along with testing frameworks like Mocha. It provides several interfaces for making 
assertions, including expect, should, and assert.
Example:
```
// app.js
function add(a, b) {
  return a + b;
}

// test.js
const chai = require("chai");
const expect = chai.expect;
const add = require("./app");

describe("add", function() {
  it("should add two numbers together", function() {
    const result = add(2, 3);
    expect(result).to.equal(5);
  });

  it("should return NaN if either argument is not a number", function() {
    const result1 = add("2", 3);
    expect(result1).to.be.NaN;

    const result2 = add(2, "3");
    expect(result2).to.be.NaN;
  });
});
```