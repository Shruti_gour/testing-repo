import sinon from "sinon";
import chai from "chai";
import { expect } from "chai";
import { addUserData } from "../controllers/userController.js";
import { addUserDataService } from "../services/addUserDataService.js";


describe("addUserData", () => {
  it("should return an error message if any field is missing", async () => {
    const request = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        education: "Test Education",
      },
    };
    const response = {
      send: sinon.spy(),
    };

    await addUserData(request, response);

    expect(response.send.calledOnce).to.be.true;
    expect(response.send.firstCall.args[0]).to.equal(
      " All fields are required!"
    );

    const hashedPassword = await bcrypt.hash("testpassword", saltRounds);
    const requestWithPassword = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        city: "Test City",
        education: "Test Education",
      },
    };
    const result = await addUserDataService(requestWithPassword);
    expect(result.password).to.equal(hashedPassword);
  });

  it("should call the addUserDataService with the correct parameters", async () => {
    const request = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        city: "Test City",
        education: "Test Education",
      },
    };
    const response = {
      send: sinon.spy(),
    };
    const addUserDataServiceStub = sinon.stub(
      addUserDataService,
      "addUserDataService"
    );

    await addUserData(request, response);

    expect(addUserDataServiceStub.calledOnce).to.be.true;
    expect(addUserDataServiceStub.firstCall.args[0]).to.deep.equal(request);

    addUserDataServiceStub.restore();
  });

  it("should return a success message when user is successfully created", async () => {
    const request = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        city: "Test City",
        education: "Test Education",
      },
    };
    const response = {
      send: sinon.spy(),
    };
    sinon.stub(addUserDataService, "addUserDataService").resolves({
      message: "Successful registration",
    });

    await addUserData(request, response);

    expect(response.send.calledOnce).to.be.true;
    expect(response.send.firstCall.args[0]).to.deep.equal({
      message: "Successful registration",
    });

    addUserDataService.addUserDataService.restore();
  });

  it("should return an error message when user already exists", async () => {
    const request = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        city: "Test City",
        education: "Test Education",
      },
    };
    const response = {
      send: sinon.spy(),
    };
    sinon.stub(addUserDataService, "addUserDataService").resolves({
      error: "User already exists",
    });

    await addUserData(request, response);

    expect(response.send.calledOnce).to.be.true;
    expect(response.send.firstCall.args[0]).to.deep.equal({
      error: "User already exists",
    });

    addUserDataService.addUserDataService.restore();
  });

  it("should return an error message when user creation is unsuccessful", async () => {
    const request = {
      body: {
        emailId: "test@test.com",
        password: "testpassword",
        name: "Test User",
        city: "Test City",
        education: "Test Education",
      },
    };
    const response = {
      send: sinon.spy(),
    };
    sinon.stub(addUserDataService, "addUserDataService").resolves({
      message: "Unsuccessful registration",
    });

    await addUserData(request, response);

    expect(response.send.calledOnce).to.be.true;
    expect(response.send.firstCall.args[0]).to.deep.equal({
      error: "User creation failed",
    });

    addUserDataService.addUserDataService.restore();
  });
});
