import jwt from "jsonwebtoken";
import { User } from "../models/user.js";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
dotenv.config();

export const loginUserDataService = async (request) => {
  const { emailId, password } = request.body;

  try {
    const user = await User.findOne({ where: { emailId } });
    if (!user) {
      return { error: "User not found" };
    }

    const passwordMatch = await bcrypt.compare(password, user.password);
    if (!passwordMatch) {
      return {
        error: "Incorrect password",
      };
    }

    const payload = {
      emailId: user.emailId,
    };

    console.log(process.env.EXPIRES_IN);

    const token = jwt.sign(payload, process.env.SECRET_TOKEN_KEY, {
      expiresIn: process.env.EXPIRES_IN,
    });
    return {
      message: "Token generated!",
      token,
    };
  } catch (error) {
    return error.message;
  }
};
